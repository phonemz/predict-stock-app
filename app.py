from flask import Flask, render_template, request, jsonify
import pickle
from sklearn.preprocessing import MinMaxScaler

app = Flask(__name__, template_folder="template")

# Load the model and scaler
model = pickle.load(open("model.pkl", "rb"))
scaler = MinMaxScaler()


@app.route("/")
def home():
    return render_template("index.html")


@app.route("/predict", methods=["POST"])
def predict():
    try:
        open_price = float(request.form["open"])
        low_price = float(request.form["low"])
        high_price = float(request.form["high"])
        volume = float(request.form["volume"])
    except ValueError as e:
        return render_template(
            "index.html",
            prediction_text="Please provide valid numerical values for all input fields.",
        )

    # Fit the scaler
    input_data = scaler.fit_transform([[open_price, low_price, high_price, volume]])

    # Make prediction
    prediction = model.predict(input_data)

    return render_template(
        "index.html", prediction_text=f"Predicted Close Price: {prediction[0]:.2f}"
    )


@app.route("/predict_ajax", methods=["POST"])
def predict_ajax():
    try:
        open_price = float(request.form["open"])
        low_price = float(request.form["low"])
        high_price = float(request.form["high"])
        volume = float(request.form["volume"])
    except ValueError as e:
        return jsonify(
            {"error": "Please provide valid numerical values for all input fields."}
        )

    # Fit the scaler
    input_data = scaler.fit_transform([[open_price, low_price, high_price, volume]])

    # Make prediction
    prediction = model.predict(input_data)

    return jsonify({"prediction": f"{prediction[0]:.2f}"})


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=9001, debug=True)
